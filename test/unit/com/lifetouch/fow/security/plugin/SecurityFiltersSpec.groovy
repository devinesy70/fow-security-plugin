package com.lifetouch.fow.security.plugin

import com.lifetouch.fow.security.controller.FowSecurityController
import grails.plugins.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.springframework.beans.factory.config.MethodInvokingFactoryBean
import spock.lang.Specification

// Drive test against LookupController since it is about the simplest controller method we have. We just need
// a controller to pick on for exercising the Filter

@TestFor(FowSecurityController)
@Mock(SecurityFilters)
class SecurityFiltersSpec extends Specification {

    def "Prevents unauthorized Ajax requests"() {
        given:
        request.addHeader('X-Requested-With', 'XMLHttpRequest')
        defineBeans {
            springSecurityService(MethodInvokingFactoryBean) {
                targetObject = this
                targetMethod = "mockSpringSecurityService"
                arguments = [false]
            }
        }

        when:
        withFilters(action: 'search') {
            controller.index()
        }

        then:
        response.status == 403
    }

    def "Allows authorized Ajax requests"() {
        given:
        request.addHeader('X-Requested-With', 'XMLHttpRequest')
        defineBeans {
            springSecurityService(MethodInvokingFactoryBean) {
                targetObject = this
                targetMethod = "mockSpringSecurityService"
                arguments = [true]
            }
        }

        when:
        withFilters(action: 'search') {
            controller.index()
        }

        then:
        response.status == 200
    }

    SpringSecurityService mockSpringSecurityService(boolean isLoggedIn) {
        SpringSecurityService securityService = Mock(SpringSecurityService)
        securityService.isLoggedIn() >> isLoggedIn
        securityService
    }
}

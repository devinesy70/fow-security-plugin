Ext.require(['Ext.Ajax.*']);

Ext.onReady(function() {
  Ext.Ajax.on('requestexception', function(proxy, response, options) {
    var loadingMask = Ext.getCmp('loading_mask');
    if (loadingMask) {
      loadingMask.hide();
    }
    if(response.status == 403) {
      delete options.callback;
      new Ext.LoadMask(Ext.getBody(), {msg:"Session Expiration Notice. Please wait while your browser window is being refreshed..."}).show();
      new Ext.util.DelayedTask(function () {
        window.location.reload();
      }).delay(4000);
    }
  });
});

package com.lifetouch.fow.security.controller

// For unit test support
class FowSecurityController {

    def index() {
        render('FowSecurityController found')
    }
}

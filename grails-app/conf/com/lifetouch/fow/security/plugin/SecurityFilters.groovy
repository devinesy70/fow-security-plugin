package com.lifetouch.fow.security.plugin

import grails.plugins.springsecurity.SpringSecurityService

class SecurityFilters {

	SpringSecurityService springSecurityService
	
    def filters = {
	
        preventUnauthorizedAjaxRequests(action:'*', view:'*', controllerExclude:'monitor') {
            before = {
                if(request.xhr && !springSecurityService.isLoggedIn()) {
                    response.status = 403
                    render("No longer authorized. Please refresh your browser window and retry")
                    return false
                }
            }
            after = { Map model ->
            }
            afterView = { Exception e ->
            }
        }
    }
}

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.repos.'lifetouch-releases'.url = "http://nexus.lifetouch.net/content/repositories/lifetouch-releases"
grails.project.repos.'lifetouch-releases'.type = "maven"
grails.project.repos.'lifetouch-snapshots'.url = "http://nexus.lifetouch.net/content/repositories/lifetouch-snapshots"
grails.project.repos.'lifetouch-snapshots'.type = "maven"
grails.project.repos.default = "lifetouch-releases"
grails.release.scm.enabled = false

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility
    repositories {
        grailsCentral()
        mavenRepo "http://nexus.lifetouch.net/content/groups/public"
    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.21'
        test (
                "org.spockframework:spock-grails-support:0.7-groovy-2.0",
                "org.objenesis:objenesis:1.3")
    }

    plugins {
        build(":tomcat:$grailsVersion",
                ":release:2.2.1",
                ":rest-client-builder:1.0.3") {
            export = false
        }

        compile(':spring-security-core:1.2.7.3') {
            export = false
        }

        test(":spock:0.7") { excludes "spock-grails-support" }
    }
}
